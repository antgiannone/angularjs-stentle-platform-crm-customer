import { StentlePlatformCrmCustomerNgSdkPage } from './app.po';

describe('stentle-platform-crm-customer-ng-sdk App', () => {
  let page: StentlePlatformCrmCustomerNgSdkPage;

  beforeEach(() => {
    page = new StentlePlatformCrmCustomerNgSdkPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
