import { Component, OnInit } from '@angular/core';

import { StentleCrmLead } from './stentle-crm-lead';
import { StentleCrmLeadsResponse } from './stentle-crm-leads-response';
import { StentleCrmCustomerServiceService } from './stentle-crm-customer-service.service';

@Component({
  selector: 'app-root',
  providers: [StentleCrmCustomerServiceService],
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'app works!';
  leadsResponse = null;
  lead = null;

  constructor(private crmCustomerServiceService: StentleCrmCustomerServiceService) { }

  getAllLeads(): Promise<StentleCrmLeadsResponse> {
    return this.crmCustomerServiceService.getLeads();
  }

  captureLead(lead: StentleCrmLead): Promise<StentleCrmLead> {
    return this.crmCustomerServiceService.captureLead(lead);
  }

  ngOnInit(): void {

    let lead = new StentleCrmLead();

    lead.email = "antonino1.giannone@gmail.com";
    lead.fullName = "Antonino Giannone";
    lead.channel = "STENTLE_PORTAL";

    this.lead = this.captureLead(lead);

    this.lead
      .then(result => console.log(result))
      .catch(error => console.log(error));

    this.leadsResponse = this.getAllLeads();

    this.leadsResponse
      .then(result => console.log(result))
      .catch(error => console.log(error));
  }
}
