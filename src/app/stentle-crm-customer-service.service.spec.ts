import { TestBed, inject } from '@angular/core/testing';

import { StentleCrmCustomerServiceService } from './stentle-crm-customer-service.service';

describe('StentleCrmCustomerServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [StentleCrmCustomerServiceService]
    });
  });

  it('should ...', inject([StentleCrmCustomerServiceService], (service: StentleCrmCustomerServiceService) => {
    expect(service).toBeTruthy();
  }));
});
