import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, URLSearchParams } from '@angular/http';

import 'rxjs/add/operator/toPromise';

import { StentleCrmLead } from './stentle-crm-lead';
import { StentleCrmLeadsResponse } from './stentle-crm-leads-response';

@Injectable()
export class StentleCrmCustomerServiceService {

  private headers: Headers;
  private options: RequestOptions;
  private crmLeadEndPoint = 'http://localhost:8989/leads.json';

  constructor(private http: Http) {

    this.headers = new Headers({ 'Content-Type': 'application/json', 'Accept': 'q=0.8;application/json;q=0.9' });
    this.options = new RequestOptions({ headers: this.headers });
  }

  getLeads(): Promise<StentleCrmLeadsResponse> {

    return this.http.get(this.crmLeadEndPoint)
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);
  }

  captureLead(lead: StentleCrmLead): Promise<StentleCrmLead> {

    return this.http.post(this.crmLeadEndPoint, JSON.stringify(lead), this.options)
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);
  }

  private extractData(res: Response) {
    const body = res.json();
    return body || {};
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }
}
