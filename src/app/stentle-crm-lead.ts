export class StentleCrmLead {
  uuid: string;
  fullName: string;
  email: string;
  channel: string;
}
