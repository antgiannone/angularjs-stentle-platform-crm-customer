import { StentleCrmLead } from './stentle-crm-lead';

export class StentleCrmLeadsResponse {

  content: Array<StentleCrmLead>;
  last: boolean;
  totalPages: number;
  totalElements: number;
  numberOfElements: number;
  sort: number;
  first: boolean;
  size: number;
  number: number;

  constructor() {

    this.content = new Array<StentleCrmLead>();
    this.last = false;
    this.totalPages = 0;
    this.totalElements = 0;
    this.numberOfElements = 0;
    this.sort = 0;
    this.first = false;
    this.size = 0;
    this.number = 0;
  }
}
